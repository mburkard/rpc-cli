"""Expose RPC methods over CLI."""

__all__ = ("cli",)

from rpc_cli._cli import cli
