"""RPC-CLI test."""

from enum import auto, Enum

from openrpc import ParamStructure, RPCServer
from pydantic import BaseModel

rpc = RPCServer()


class Flavor(Enum):
    """Flavors."""

    MOCHA = "mocha"
    VANILLA = auto()


class Vector3(BaseModel):
    x: float
    y: float
    z: float


# test_rpc_cli.py get_distance '{"x": 1, "y": 1, "z": 1}' '{"x": 1, "y": 1, "z": 1}'
@rpc.method()
def get_distance(a: Vector3, b: Vector3) -> Vector3:
    """Get distance between two points."""
    return Vector3(x=a.x - b.x, y=a.y - b.y, z=a.z - b.z)


# test_rpc_cli.py get_distance 1
# test_rpc_cli.py get_distance 1 1
# test_rpc_cli.py get_distance 1 0
@rpc.method()
def divide(a: int, b: int = 2) -> float:
    """Divide two integers."""
    return a / b


@rpc.method()
def concat(a: str, b: str = "2") -> str:
    """Concatenate two strings."""
    return a + b


# test_rpc_cli.py summation '[1, 2, 3]'
@rpc.method()
def summation(numbers: list[int | float]) -> int | float:
    """Sum all numbers in a list."""
    return sum(numbers)


@rpc.method(param_structure=ParamStructure.BY_NAME)
def add(a: int, b: int) -> int:
    """Add two integers."""
    return a + b


@rpc.method()
def echo(flavor: Flavor) -> Flavor:
    """Echo flavor input."""
    return flavor


@rpc.method()
async def async_add(a: int, b: int) -> int:
    """Async add method."""
    return a + b
