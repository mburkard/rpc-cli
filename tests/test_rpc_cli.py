"""Unit tests for the RPC API."""

from inspect import cleandoc
from cleo.testers.command_tester import CommandTester

from rpc_cli import cli
from tests.rpc_api import rpc


def test_objects() -> None:
    input_ = """'{"x": 1, "y": 1, "z": 1}' '{"x": 1, "y": 1, "z": 1}'"""
    expected = cleandoc("""
        {
          "x": 0.0,
          "y": 0.0,
          "z": 0.0
        }
    """)
    assert _test_command("get_distance", input_) == expected


def test_default() -> None:
    assert _test_command("divide", "1") == "0.5"


def test_error() -> None:
    expected = cleandoc("""
        {
          "code": -32000,
          "message": "Server error"
        }
    """)
    assert _test_command("divide", "1 0") == expected


def test_strings() -> None:
    assert _test_command("concat", "a b") == '"ab"'
    assert _test_command("concat", "1 1") == '"11"'


def test_array() -> None:
    assert _test_command("summation", "'[2, 4, 6]'") == "12"


def test_by_name() -> None:
    assert _test_command("add", "1 1") == "2"


def test_enum() -> None:
    assert _test_command("echo", "mocha") == '"mocha"'
    assert _test_command("echo", "1") == "1"


def test_async() -> None:
    assert _test_command("async_add", "1 2") == "3"


def _test_command(cmd: str, input_: str) -> str:
    application = cli(rpc)
    command = application.find(cmd)
    command_tester = CommandTester(command)
    command_tester.execute(input_)
    return command_tester.io.fetch_output().removesuffix("\n")
